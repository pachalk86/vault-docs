
* To Run Vault in Web UI
- Install vault from official repo . Visit the below link

   [Download Link](https://learn.hashicorp.com/tutorials/vault/getting-started-install)

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install vault
```
- Verify vault installation

``` 
vault -version 
```  
- Add the below configuration inside /etc/vault/vault.hcl before running vault server

```  ui = true ```
- If you are running in non ssl mode i.e, in http mode than enable below line in same /etc/vault/vault.hcl  file
```
 listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = 1
} 
storage "file" {
  path = "/opt/vault/data"
}

api_addr = "http://0.0.0.0:8200"

```


- Save the above config file

- Restart the Vault Service

```
sudo systemctl restart vault
```
- If service file doesnot exists then, Make service file
- Inside ```sudo vim  /etc/systemd/system/vault.service ```

```
[Unit]
Description=Vault dev server

[Service]
ExecStart=vault server -config=/etc/vault.d/vault.hcl
```
- Now start service using

```
sudo systemctl reload deamon
sudo systemctl start vault
```
- Check your service web UI in ```http://ip:8200```