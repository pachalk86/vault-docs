# Vault inside Docker-Compose

## A Small Intro

We are using vault to store our secrets in a singular, vault server makes it easy to store different type of secrets inside it. It's is a tool for securely accessing secrets, a secret such as API keys, passwords, or certificates. 
You can create entities (users) , groups and assign policies such as read access of secret or create secret or delete any secret.

We can always get secrets from a api call

```
curl \
    --header "X-Vault-Token: ..." \
    --request POST \
    --data @payload.json \
    https://127.0.0.1:8200/v1/secret/config
```

## Docker-Compose Setup

* Your vault.hcl file will look something like this. save the below file inside vault/config

```
mkdir -p vault/config 
vim vault/config/vault.hcl
```

```
# Full configuration options can be found at https://www.vaultproject.io/docs/configuration

ui = true

storage "file" {
  path = "/opt/vault/data"
}

# HTTP listener
listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = 1
}

```

* Your Docker Compose yaml file

```
version: '3'
services:
  vault:
    image: vault:latest
    volumes:
      - ./vault/config:/vault/config
      - ./vault/policies:/vault/policies
      - ./vault/data:/vault/data
    ports:
      - 8200:8200
    environment:
      - VAULT_ADDR=http://0.0.0.0:8200
      - VAULT_API_ADDR=http://0.0.0.0:8200
      - VAULT_ADDRESS=http://0.0.0.0:8200
    cap_add:
      - IPC_LOCK
    command: 
       vault server -config=/vault/config/vault.hcl
```
* now in broswser hit ```localhost:8200```

## Why it's not recommended using same compose as of your service for Vault

* This type of setup means you are creating secrets runtime for everytime you do ```docker compose --build```
* You will be using those same created secrets in same docker compose file

```
services:
  vault:
    image: vault:latest
    container_name: vault
    volumes:
      - ./vault/config:/vault/config
      - ./vault/policies:/vault/policies
      - ./vault/data:/vault/data
    ports:
      - 8200:8200
    environment:
      - VAULT_ADDR=http://0.0.0.0:8200 
  app:
    image: nginx:latest
    ports:
      - 80:80
    command:
        docker inspect -f '{{range $index, $value := .Config.Env}}export {{$value}}{{println}}{{end}}' vault | grep DB_USER
```
something like above but this also needs some workaround to work perfectly.

* After finishing the above setup even though we will have to share pur secrets into vault service hardcoded and in json format. For example:-

secret.json
```
[
  {
    "vault_path": "kv/app/database",
    "version": -2,
    "set": {
      "DB_HOST": "3306",
      "DB_USER": "admin",
      "DB_PASSWORD": "admin@123"
    }
  }
]
```


```
services:
  vault:
    image: vault:latest
    environment:
      - VAULT_ADDR=http://0.0.0.0:8200
      - SECRET_CONFIG_FILE=/config/secret_config.json
```

So by this also we will use a secret file with hardcoded secret values.

## Steps needs to be done while using vault inside docker compose

* Creating vault service inside compose file with configuration

* Generate root token on runtime (which gets saved in and Environment varible i.e, VAULT_TOKEN)

* Creating the secrets inside the vault container using same docker compose 

* Creating Entity/users inside vault container & attaching policies 

* Once these steps are done we will be using vault api calls to get secrets

```
curl \
    --header "X-Vault-Token: ..." \
    https://127.0.0.1:8200/v1/secret/data/my-secret?version=2

```

## Challenges Facing in this setup

* To generate Root token autimatically via compose command but right now we are currently have to go inside the container and run cmd

* If we are success in creating root token then also we will have to create secret.json file with all secrets written inside as plain text mainly. for ex:-

```
[
  {
    "vault_path": "kv/app/database",
    "version": -2,
    "set": {
      "DB_HOST": "3306",
      "DB_USER": "admin",
      "DB_PASSWORD": "admin@123"
    }
  }
]
```