#### This will let you know how to create secret

#### Access to link ```http://ip:8200/```

#### Now create key

#### Now download the file

#### Unseal Vault from vault keys

![](./img/unseal.png)

#### these keys

![](./img/keys.png)

#### Sign in using token

![](./img/tokenlogin.png)

#### Click create Secret engine

![](./img/secret_engine.png)

#### Then go inside secret and create new give path as ```appserver```

![](./img/secret1.png)

#### In same way create second secret

![](./img/secret21.png)

#### You can see all the secrets

![](./img/listofsecret.png)

#### It will look something lihe this

![](./img/secrets_created.png)

#### Create policies which provides read only access

![](./img/appreadonly.png)

#### For database also

![](./img/createsbaccess.png)

#### Your Acl Policies

![](./img/aclpolicies.png)

#### Create Auth method For ```1timeOnly```

![](./img/authmethod.png)

#### Authentication Method are these

![](./img/method.png)

#### Create Entity from entity menu

![](./img/entitycreate.png)

#### Create alias inside entity only. choose userpass as authentication method. you require this to get login for user

![](./img/aliases1.png)


![](./img/aliases2.png)


#### Your alias Is added

![](./img/addalias.png)


#### Now we will create a group and user who will have the read only access of app server

![](./img/create_group.png)

#### In a similar way we created 2 groups

![](./img/2group.png)

####  Edit Group and add Member entity 

![](./img/chooseentitymember.png)

#### Add ```Export``` In CLI vault server

```
export VAULT_TOKEN="s.dsfhiuhafdhalsfhaoif32"
export VAULT_ADDR="http://ip:8200/"

```

#### Once done please add password to those user thorugh CLI

```
vault write auth/userpass/users/app_user/ password="app@123"
vault write auth/userpass/users/db_user/ password="db@123"

```
![](./img/clicmd.png)


``` Hurray !! that's it you can login into those users from UI```

![](./img/loginapp.png)

#### Access Denied on the secrets you are not allowed for app user you cannot see db_secrets

![](./img/accessdenied.png)

#### Now access granted on db secret for db user

![](./img/dbaccess.png)